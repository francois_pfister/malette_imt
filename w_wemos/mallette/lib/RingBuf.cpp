#include "RingBuf.h"

#include <Arduino.h>




RingBuf::RingBuf( int size )
{
	bufferSize = size;
	bufferTail = 0;
	bufferHead = 0;
	buffer_p = ( uint8_t* )malloc( size );
	buffer = ( uint8_t* )malloc( size );
    memset( buffer_p, 0, size );
    memset(buffer, 0, size);
}

RingBuf::~RingBuf()
{
	if( buffer_p )
          free( buffer_p );
	if( buffer )
          free( buffer );
}

// public functions

int RingBuf::available(void){
  int ByteCount = (bufferSize + bufferHead - bufferTail) % bufferSize;
  return ByteCount;
}

int RingBuf::read(void) {
  if (bufferHead == bufferTail) {
    return -1;
  }
  else {
    uint8_t c = buffer_p[bufferTail];
    bufferTail = (bufferTail + 1) % bufferSize;
    if(bufferHead == bufferTail) {
      bufferTail = 0;
      bufferHead = 0;
    }
    return c;
  }
}


uint8_t* RingBuf::content(void){
	int n = 0;
    memset(buffer, 0, 255);
	while (available() && n < 255)
		buffer[n++] = read();
	return buffer;
}


uint8_t* RingBuf::snapshot(void){
	int n = 0;
    memset(buffer, 0, 255);
	while (available() && n < 255)
		buffer[n++] = read();
	print((char*)buffer);
	return buffer;
}


uint8_t RingBuf::write( int c )
{ 
  if ((bufferHead + 1) % bufferSize == bufferTail) {
    return -1;
  }
  buffer_p[bufferHead] = c;
  bufferHead = (bufferHead + 1) % bufferSize;
  return 0;
}

int RingBuf::print(const char* s){
   for (int i=0;i<strlen(s);i++)
	   if (write(s[i]) !=0)
		   return -1;
   return 0;
}


void RingBuf::clear(){
	while (available())
		read();
}


int RingBuf::println(const char* s){
   if (print(s)!=0)
		   return -1;
   if (print("\r\n")!=0)
		   return -1;
   return 0;
}


void RingBuf::remove(int n)
{
  if(bufferHead != bufferTail) {
    bufferTail = (bufferTail + n) % bufferSize;
  }
}

int RingBuf::peek(void)
{
  if (bufferHead == bufferTail) {
    return -1;
  }
  else {
    return buffer_p[bufferTail];
  }
}

int RingBuf::peek(int n) {

  if (bufferHead == bufferTail) {
    return -1;
  }
  else {
    return buffer_p[(bufferTail + n) % bufferSize];
  }
}
