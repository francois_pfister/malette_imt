
#include <stdint.h>

#ifndef RingBuf_h
#define RingBuf_h


class RingBuf{
private:

	int bufferSize;
	unsigned int bufferHead, bufferTail;
	uint8_t *buffer_p;
	uint8_t *buffer;
	
public:
	RingBuf(int size);
        ~RingBuf();
	int available(void);
	int peek(void);
	int peek(int);
	void remove(int);
	int read(void);
	uint8_t write(int);
	int print (const char*);
	int println(const char*);
	uint8_t* content(void);
	void clear();
	uint8_t* snapshot(void);
};
#endif
