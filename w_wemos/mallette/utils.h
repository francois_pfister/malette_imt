/*
 * utils.h
 *
 *  Created on: 24 avr. 2021
 *      Author: pfister@connecthive.com
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <Arduino.h>

#ifdef __cplusplus
extern "C" {
#endif


String urlencode(String str);
char* substring(char *destination, const char *source, int beg, int n);
void scan_i2c();
void mac2hex(uint8_t * mac, char * dest);
void split(char* str, const char* delim, char** res, int sizemax);

#ifdef __cplusplus
} /* extern "C" */
#endif



#endif /* UTILS_H_ */
