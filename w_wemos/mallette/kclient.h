/*
 * client.h
 * wrapper for http client
 *  Created on: 20 oct. 2019
 *      Author: pfister@connecthive.com
 */

#ifndef KCLIENT_H_
#define KCLIENT_H_


#define LOOP_IP_DELAY 5 //30

#define SEND_UNDEFINED 0
#define SEND_EMON 1
#define SEND_REST_ 2
#define SEND_IOT 3
#define SEND_MQTT_ 4

#ifdef __cplusplus
extern "C" {
#endif

  void sendIpData_1();
  void sendIpData_2();

#ifdef __cplusplus
} /* extern "C" */
#endif

extern const char* connecthive_host_1;
extern const char* app_connecthive_1;
extern const int connecthive_httpPort_1;


extern const char* connecthive_host_2;
extern const char* app_connecthive_2;
extern const int connecthive_httpPort_2;



#endif /* KCLIENT_H_ */
