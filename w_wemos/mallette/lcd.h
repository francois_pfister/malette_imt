/*
 * lcd.h
 *  Wrapper for LCD
 *  Created on: 20 oct. 2017
 *      Author: pfister@connecthive.com
 */

#ifndef LCD_H_
#define LCD_H_

//#include <IPAddress.h>


#ifdef __cplusplus
extern "C" {
#endif



void setupLcd();
void lcdLoop();
void lcdWrite(int x, int y, const char* message, int trace);
void lcdMessage(const char* mesg1, const char* mesg2);
void lcdMessage2(const char* mesg1, String mesg2);


#ifdef __cplusplus
} /* extern "C" */
#endif



#endif /* LCD_H_ */
