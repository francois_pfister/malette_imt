/*
 * utils.cpp
 *
 *  Created on: 24 avr. 2021
 *      Author: pfister@connecthive.com
 */

#include "utils.h"

#include <Wire.h>

char int2Hex(uint8_t v) {
	return (v < 10) ? ('0' + v) : ('A' + (v - 10));
}

void uint8_2hex(uint8_t v, char * dest) {
	dest[0] = int2Hex((v & 0xF0) >> 4);
	dest[1] = int2Hex(v & 0x0F);
	dest[2] = '\0';
}

void mac2hex(uint8_t * mac, char * dest) {
	for (int j = 0; j < 6; j++)
		uint8_2hex(mac[j], &dest[2 * j]);
	dest[13] = '\0';
}

String urlencode(String str) {
	char c;
	char code0;
	char code1;
	//char code2;
	String encodedString = "";
	for (unsigned int i = 0; i < str.length(); i++) {
		c = str.charAt(i);
		if (c == ' ') {
			encodedString += '+';
		} else if (isalnum(c)) {
			encodedString += c;
		} else {
			code1 = (c & 0xf) + '0';
			if ((c & 0xf) > 9) {
				code1 = (c & 0xf) - 10 + 'A';
			}
			c = (c >> 4) & 0xf;
			code0 = c + '0';
			if (c > 9) {
				code0 = c - 10 + 'A';
			}
			//code2 = '\0';
			encodedString += '%';
			encodedString += code0;
			encodedString += code1;
			//encodedString+=code2;
		}
		// yield();
	}
	return encodedString;
}

char* substring(char *destination, const char *source, int beg, int n) {
	while (n > 0) {
		*destination = *(source + beg);
		destination++;
		source++;
		n--;
	}
	*destination = '\0';
	return destination;
}

void split(char* str, const char* delim, char **res, int sizemax) {
		char *ptr = strtok(str, delim);
        int j = 0;
		while (ptr != NULL) {
			if (j==sizemax)
				 return;
			res[j++] = ptr;
			ptr = strtok(NULL, delim);
		}
	}

void scan_i2c() {
	byte error, address;
	int nDevices;

	Serial.println("Scanning...");

	nDevices = 0;
	for (address = 1; address < 127; address++) {
// The i2c_scanner uses the return value of
// the Write.endTransmisstion to see if
// a device did acknowledge to the address.
		Wire.beginTransmission(address);
		error = Wire.endTransmission();

		if (error == 0) {
			Serial.print("I2C device found at address 0x");
			if (address < 16)
			Serial.print("0");
			Serial.print(address, HEX);
			Serial.println(" !");

			nDevices++;
		} else if (error == 4) {
			Serial.print("Unknow error at address 0x");
			if (address < 16)
			Serial.print("0");
			Serial.println(address, HEX);
		}
	}
	if (nDevices == 0)
	Serial.println("Noz I2C devices found\n");
	else
	Serial.println("done\n");

}

