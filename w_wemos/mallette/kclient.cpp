/*
 * client.cpp
 * wrapper for http client
 *  Created on: 20 oct. 2019
 *      Author: pfister@connecthive.com
 */

#include "defdebug.h"
#include <ESP8266WiFi.h>
#include "kclient.h"
#include "utils.h"
#include "variables.h"



#define WS_APP_1 "connecthiweb"
#define WS_APP_2 ""

#define  HTTP_PORT_1  8080
#define  HTTP_PORT_2  9090


WiFiClient wclient;

//testing with a localhost server

const char* connecthive_host_1 = "192.168.1.50";
const int connecthive_httpPort_1 = HTTP_PORT_1;

const char* connecthive_host_2 = "192.168.1.50";
const int connecthive_httpPort_2 = HTTP_PORT_2;


const int privateKey = 123456;
const char* nodeId = "21";

const char* app_connecthive_1 = WS_APP_1;
const char* app_connecthive_2 = WS_APP_2;



#define LOG_DATA_3 false


void request_k_1(String req) { // @suppress("Abstract class cannot be instantiated")
	DEBUG_PRINTLN("\nRequesting: ");
	DEBUG_PRINT("http://");
	DEBUG_PRINT(connecthive_host_1);
	DEBUG_PRINT(":");
	DEBUG_PRINT(connecthive_httpPort_1);
	DEBUG_PRINTLN(req);
	wclient.print(
			String("GET ") + req + " HTTP/1.1\r\n" + "Host: " + connecthive_host_1
					+ "\r\n" + "User-Agent: ConnecthiveESP32\r\n"
					+ "Connection: close\r\n\r\n");
	delay(10);
	while (wclient.available()) {
		String line = wclient.readStringUntil('\r');
		DEBUG_PRINTLN(line);
	}
	DEBUG_PRINTLN("closing connection");
}


void send_data_tcp(const char *host, const int port, const char *application,bool encode) {
	DEBUG_PRINTLN("connecting to ");
	DEBUG_PRINT("http://");
	DEBUG_PRINT(host);
	DEBUG_PRINT(":");
	DEBUG_PRINT(port);
	if (!wclient.connect(host, port)) {
		DEBUG_PRINTLN("\nconnection (2) failed");
		return;
	}

	String url = "/";
	if (strlen(application)>0){
		   url += application;
		   url += "/";
	}

	url += "post.json?node=";
	url += nodeId;
	url += "&apikey=";
	url += privateKey;
	if (encode) { //not device for emoncms
		url += "&device=";
		url += device_id;
		url += "&comment=";
		url += "na";
	}
	if (encode)
		json_data = urlencode(json_data); //not encode for emoncms

	url += "&json=";
	url += json_data;
	DEBUG_PRINTLN("\nRequesting: ");
	DEBUG_PRINT("http://");
	DEBUG_PRINT(host);
	DEBUG_PRINT(":");
	DEBUG_PRINT(port);
	DEBUG_PRINTLN(url);
	// This will send the request to the server
	wclient.print(
			String("GET ") + url + " HTTP/1.1\r\n" + "Host: " + host + "\r\n"
					+ "Connection: close\r\n\r\n");
	delay(100);
	// Read all the lines of the reply from server and print them to Serial
	while (wclient.available()) {
		String line = wclient.readStringUntil('\r');
		DEBUG_PRINTLN(line);
	}
	DEBUG_PRINTLN();
	DEBUG_PRINTLN("closing connection");

	/*
	//FP200128
	http://192.168.1.50:8080/connecthiweb/post.json?node=21&apikey=123456&device=5ECF7FC2605F&comment=na&json=%7Btemperature%3A0%2E00%2Chumidity%3A0%2E00%2Cvcc%3A65535%2Cambient%3A0%2E00%2Cobject%3A0%2E00%2Cweight%3A102961%7D
	*/
}



void sendIpData_1() {
   send_data_tcp(connecthive_host_1, connecthive_httpPort_1, app_connecthive_1,true);
}

void sendIpData_2() {
   send_data_tcp(connecthive_host_2, connecthive_httpPort_2, app_connecthive_2,true);
}



