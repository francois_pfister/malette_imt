
/*
 * mallette_01.cpp
 *  LabCase for IOT experiments
 *  Created on: 20 oct. 2017
 *      Author: pfister@connecthive.com
 */

#include <SoftwareSerial.h>
#include <string.h>
#include <I2CScanner.h>
#include "lib/RingBuf.h"
#include "lcd.h"
#include "kwifi.h"
#include "kclient.h"
#include "kinflux.h"
#include "utils.h"
#include "variables.h"


#define MODE_IP false
#define INFLUXDB true

#define W_TX_PIN  D6
#define W_RX_PIN  D7
#define SERIAL_TIMEOUT 1000


#define CMD_SLEEP "/sleep"
#define SLEEP false

const int private_Key = 123456;
const char* node_Id = "21";
bool wifiDisabled = true;
SoftwareSerial softwareSerial(W_RX_PIN, W_TX_PIN, false); // @suppress("Abstract class cannot be instantiated")
I2CScanner scanner;
RingBuf combuf(256);
RingBuf inBuffer(256);
int cmp = 1;

float adc = 0.f;
int btn_ = 78;
char device_id[30] = "5ECF7F000001";

int temperature = 100;
int humidity = 100;
char mbuf[24] = { '\0' };

int vcc=0;
String json_data = "";

void deep_sleep(long duration);
void make_json_data();
void sendLocalData();


//parse serial frames coming from the nucleo
// eg:
// adc_read=0.145
// button_read=31

int parse(char *txt) {
	btn_ = 0;
	int result = -1;
	strcpy(mbuf, txt);
	char *token1 = strtok(txt, "=");
	char *token2 = strtok(NULL, "=");
	if (token1 != NULL && token2 != NULL) {
		if (strcmp(token1, "adc_read") == 0) {
			adc = atof(token2);
			result = 1;
		} else if (strcmp(token1, "button_read") == 0) {
			btn_ = atoi(token2);
			if (btn_!=78){
				btn_ -= 30;
			  result = 2;
			}
		}
	}
	if (result > 0) {
		char b[101];
		sprintf(b, "\n[parse adc=%f btn=%d]", adc, btn_);
		Serial.println(b);
	}
	return result;
}



bool decode() {
	char *received = (char*) (combuf.content());
	int parsed = parse(received);
	if (parsed == 1 || parsed == 2) {
		lcdMessage(" ", mbuf);
		if (cmp++ % 8 == 0) {
			if (INFLUXDB && parsed == 1)
				send_influxdb();
			make_json_data();
			if (MODE_IP)
				sendIpData_2();
		}
	}
	if (parsed == 1 || parsed == 2) {
		delay(200);
		return true; //parse(cnt);
	} else
		return false;
}

bool soft_serial_receive() {
	int timeout = SERIAL_TIMEOUT;
	while (true) {
		delay(1);
		if (timeout-- == 0) {
			//no response in delay
			return false;
		}
		int len = softwareSerial.available();
		if (len > 0) {
			uint8_t sbuf[len + 1];
			softwareSerial.readBytes(sbuf, len);
			sbuf[len] = 0;
			for (int r = 0; r < len; r++) {
				char c = sbuf[r];
				if (c != '\n' && c != '\r')
					combuf.write(c);
				else if (c == '\n')
					return decode();
			}
		}
	}
	return false;
}

bool parse_cmd() {
	char *content = (char*) inBuffer.content();
	if (strcmp(content, CMD_SLEEP) == 0)
		deep_sleep(5000L);
	else
		return false;
	return true;
}


void serial_receive() {
	int len = Serial.available();
	if (len > 0) {
		uint8_t sbuf[len + 1];
		Serial.readBytes(sbuf, len);
		sbuf[len] = 0;
		for (int r = 0; r < len; r++) {
			char c = sbuf[r];
			if (c == '/') {
				inBuffer.clear();
				inBuffer.write(c);
			} else if (c != '\n' && c != '\r')
				inBuffer.write(c);
			else if (c == '\n')
				parse_cmd();
		}
	}
}

void make_json_data() {
	float t = temperature / 10;
	float h = humidity / 10;
	long sent_measure = adc * 1000L; //weight; // + 100000L;
	json_data = "{temperature:";
	json_data += t;
	json_data += ",humidity:";
	json_data += h;
	json_data += ",button:";
	json_data += btn_;
	/*
	 json_data += ",vcc:";
	 json_data += vcc;
	 json_data += ",ambient:";
	 json_data += mlxAmbient;
	 json_data += ",object:";
	 json_data += mlxObject;*/
	json_data += ",weight:";
	json_data += sent_measure;
	json_data += "}";

}

void sim_local_send(const char *host, const int port, const char *application,
bool encode) {

	String url = "/";
	url += application;
	url += "/post.json?node=";
	url += node_Id;
	url += "&apikey=";
	url += private_Key;
	if (encode) { //not device for emoncms
		url += "&device=";
		url += device_id;
		url += "&comment=";
		url += "na";
	}
	if (encode)
		json_data = urlencode(json_data); //not encode for emoncms
	url += "&json=";
	url += json_data;
	Serial.println("\nRequesting: ");
	Serial.print("http://");
	Serial.print(host);
	Serial.print(":");
	Serial.print(port);
	Serial.println(url);
	/*
	 //FP200128
	 http://192.168.1.50:8080/connecthiweb/post.json?node=21&apikey=123456&device=5ECF7FC2605F&comment=na&json=%7Btemperature%3A0%2E00%2Chumidity%3A0%2E00%2Cvcc%3A65535%2Cambient%3A0%2E00%2Cobject%3A0%2E00%2Cweight%3A102961%7D
	 */
}

void sendLocalData() {
	sim_local_send(connecthive_host_1, connecthive_httpPort_1, app_connecthive_1,
			true);
}


void do_loop() {
	vcc = ESP.getVcc();
	serial_receive();
	soft_serial_receive();

	/*
	if (SLEEP) {
		Serial.println("Going to sleep");
		delay(250);
		deep_sleep(LOOP_IP_DELAY);
	} else {
		delay(5000);
	}*/
}

void flushRxLine() {
	if (softwareSerial.available() > 0) {
		while (softwareSerial.available()) {
			char c = softwareSerial.read();
			delay(100);
		}
	}
}

void writeLED(bool on) {
// inverted logic
	if (on) {
		digitalWrite(LED_BUILTIN, 0);
		Serial.println("LED ON");
	} else {
		digitalWrite(LED_BUILTIN, 1);
		Serial.println("LED OFF");
	}
}

void setupHardware() {
	pinMode(LED_BUILTIN, OUTPUT);
	writeLED(false);
	//static const uint8_t D4   = 2;
	//static const uint8_t D5   = 14;
    //Wire.begin(D5, D4);
}

void softwareSerialInit() {
	softwareSerial.begin(9600);
	delay(10);
	flushRxLine();
}

void do_setup() {
	Serial.begin(115200);
	Serial.setDebugOutput(false);
	combuf.clear();
	setupHardware();
	if(autoconnect())
	   getMacAddress();
}

void deep_sleep(long duration) {
		Serial.print("going to deep sleep during ");
		Serial.print(duration);
		Serial.println(" seconds");
		duration *= 1000000L;
		Serial.print("deep sleep starts");
		if (wifiDisabled)
			ESP.deepSleep(duration, WAKE_RF_DISABLED);
		else
			ESP.deepSleep(duration);
		delay(1000);
}


void setup() {
	softwareSerialInit();
	do_setup();
	setupLcd();
	setup_influxdb();
	lcdMessage("setup", "ok");
}

void loop() {
	do_loop();
}
//see https://forum.arduino.cc/t/comment-transferer-des-donnees-sans-avoir-a-actualiser-une-page-web/616187/4
//see https://corlysis.com/
