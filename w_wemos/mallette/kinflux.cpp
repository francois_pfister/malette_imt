/*
 * kinflux.cpp
 *  Wrapper for Influx DB
 *  Created on: 30 avr. 2021
 *      Author: pfister@connecthive.com
 */

#include "kinflux.h"
#include "variables.h"

#include <InfluxDbClient.h>
#include <InfluxDbCloud.h>



// InfluxDB v2 server url, e.g. https://eu-central-1-1.aws.cloud2.influxdata.com (Use: InfluxDB UI -> Load Data -> Client Libraries)
#define INFLUXDB_URL_ "https://europe-west1-1.gcp.cloud2.influxdata.com"
// InfluxDB v2 server or cloud API authentication token (Use: InfluxDB UI -> Data -> Tokens -> <select token>)
#define INFLUXDB_TOKEN_ "oGuvA8Jz-TTFAy0KOtCBqTjNmzcZRjGTGRbf4djq5rbpNw7qyct9Wo2dQCrx2FzbZmRdClQoKEWUSz3ELV9YnQ=="
// InfluxDB v2 organization id (Use: InfluxDB UI -> User -> About -> Common Ids )
#define INFLUXDB_ORG "pfister@connecthive.com"
// InfluxDB v2 bucket name (Use: InfluxDB UI ->  Data -> Buckets)
#define INFLUXDB_BUCKET "pfister's Bucket"

#define MTK2_TOKEN "s7wTHeMiiMiXfk920HRjKGFpcn87-pn0ARylaQR3wvEET2XOJ4R6SSe0BEQjDumO13apFdPB4vTb0jaErzMpmw=="

// Set timezone string according to https://www.gnu.org/software/libc/manual/html_node/TZ-Variable.html
// Examples:
//  Pacific Time: "PST8PDT"
//  Eastern: "EST5EDT"
//  Japanesse: "JST-9"
//  Central Europe: "CET-1CEST,M3.5.0,M10.5.0/3"
#define TZ_INFO "CET-1CEST,M3.5.0,M10.5.0/3"

// InfluxDB client instance with preconfigured InfluxCloud certificate
InfluxDBClient influxclient(INFLUXDB_URL_, INFLUXDB_ORG, INFLUXDB_BUCKET, MTK2_TOKEN,
		InfluxDbCloud2CACert);

// Data point
Point sensor("solar_status");

#define DEVICE "solar_panel"

int timecnt = 0;

void setup_influxdb() {
	sensor.addTag("device", DEVICE);
	timeSync(TZ_INFO, "pool.ntp.org", "time.nis.gov");
	if (influxclient.validateConnection()) {
		Serial.print("Connected to InfluxDB: ");
		Serial.println(influxclient.getServerUrl());
	} else {
		Serial.print("InfluxDB connection failed: ");
		Serial.println(influxclient.getLastErrorMessage());
	}
}

void send_influxdb() {
	Serial.print("Writing influxdb: ");
	sensor.clearFields();
	if (timecnt++%16==0){
	  Serial.println("synchronize time");
	  timeSync(TZ_INFO, "pool.ntp.org", "time.nis.gov");
	}
	sensor.addField("solar", adc);
	Serial.println(sensor.toLineProtocol());
	if (!influxclient.writePoint(sensor)) {
		Serial.print("InfluxDB write failed: ");
		Serial.println(influxclient.getLastErrorMessage());
	} else
		Serial.print("InfluxDB write success");
}
