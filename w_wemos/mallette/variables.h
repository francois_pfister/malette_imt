/*
 * variables.h
 *
 *  Created on: 24 avr. 2021
 *      Author: pfister@connecthive.com
 */

#ifndef VARIABLES_H_
#define VARIABLES_H_

#include <Arduino.h>


extern char kbbuf1;
extern char kbbuf2;


extern String json_data;
extern char device_id[];
extern bool wifiDisabled;

extern float adc;



#endif /* VARIABLES_H_ */
