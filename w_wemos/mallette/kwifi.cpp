/*
 * kwifi.cpp
 *  Wrapper for WIFI
 *  Created on: 24 avr. 2021
 *      Author: pfister@connecthive.com
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiManager.h>
#include "kwifi.h"
#include "utils.h"
#include "variables.h"
#include "defdebug.h"
#include "callbacks.h"




bool autoconnect() {
	Serial.println("autoconnect");
	//wifiDisabled = false;
	WiFi.mode(WIFI_STA); // @suppress("Method cannot be resolved")
	DEBUG_PRINTLN();
	WiFiManager wifimanager;
	wifimanager.setTimeout(120); //so if it restarts and router is not yet online, it keeps rebooting and retrying to connect
	if (!wifimanager.autoConnect("connecthive")) {
		DEBUG_PRINTLN("timeout - going to sleep");
		delay(200);
		//sleep and try again
		deep_sleep(10000);
		return false;
	}
	DEBUG_PRINTLN();
	DEBUG_PRINTLN("WiFi connected");
	DEBUG_PRINTLN("my IP address: ");

	DEBUG_PRINTLN(WiFi.localIP());
	return true;
}

void getMacAddress() {
	uint8_t mac[6]; //WL_MAC_ADDR_LENGTH
	//memset(mac, 0, 6);
	WiFi.softAPmacAddress(mac);
	char buf[50];

	mac2hex(mac, buf);

	DEBUG_PRINT("\n-----device_id = ");
	DEBUG_PRINTLN(buf);
}

void disable_wifi() {
	wifiDisabled = true;
	WiFi.mode(WIFI_OFF);
	WiFi.forceSleepBegin();
}
