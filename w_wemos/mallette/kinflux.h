/*
 * kinflux.h
 *  Wrapper for Influx DB
 *  Created on: 30 avr. 2021
 *      Author: pfister@connecthive.com
 */

#ifndef KINFLUX_H_
#define KINFLUX_H_



#ifdef __cplusplus
extern "C" {
#endif

void setup_influxdb();

void send_influxdb();


#ifdef __cplusplus
} /* extern "C" */
#endif


#endif /* KINFLUX_H_ */
