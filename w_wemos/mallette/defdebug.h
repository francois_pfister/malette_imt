/*
 * defdebug.h
 *
 *  Created on: 12 oct. 2019
 *      Author: pfister@connecthive.com
 */

#ifndef DEFDEBUG_H_
#define DEFDEBUG_H_


#define DEBUGPRINT

#ifdef DEBUGPRINT
#define DEBUG_PRINTLN(x)  Serial.println (x)
#define DEBUG_PRINT(x)  Serial.print (x)
#else
#define DEBUG_PRINTLN(x)
#define DEBUG_PRINT(x)
#endif


#endif /* DEFDEBUG_H_ */
