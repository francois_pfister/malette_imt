/*
 * a6client.cpp
 * wrapper for TinyGsm
 *  Created on: 21 avr. 2021
 *      Author: pfister@connecthive.com
 */
#include <Arduino.h>
#include "a6client.h"


#define TINY_GSM_MODEM_A6

#define TINY_GSM_DEBUG SerialMon


#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 115200

#define SerialMon Serial


#define SerialAT Serial1


#define TINY_GSM_USE_GPRS true
#define TINY_GSM_USE_WIFI false

// set GSM PIN, if any
#define GSM_PIN ""

// Your GPRS credentials
// Leave empty, if missing user or pass
//const char apn[]  = "YourAPN";
const char apn[] = "free";
const char gprsUser[] = "";
const char gprsPass[] = "";

const char server[] = "164.132.226.205";
const char resource_2[] = "/stak/deeprun.html";
const char resource_1[] = "/stak/helo";

#include <TinyGsmClient.h>

TinyGsm modem(SerialAT);

TinyGsmClient client(modem); // @suppress("Abstract class cannot be instantiated")
const int port = 8080;

void setup_a6() {
	// Set console baud rate
	SerialMon.begin(115200);
	delay(10);
	pinMode(20, OUTPUT);
	digitalWrite(20, HIGH);
	pinMode(23, OUTPUT);
	digitalWrite(23, HIGH);
	SerialMon.println("Wait...");
	SerialAT.begin(9600);
	delay(3000);

	// Restart takes quite some time
	// To skip it, call init() instead of restart()
	SerialMon.println("Initializing modem...");
	modem.restart();
	// modem.init();

	String modemInfo = modem.getModemInfo();
	SerialMon.print("Modem: ");
	SerialMon.println(modemInfo);

	// Unlock your SIM card with a PIN if needed
	if ( GSM_PIN && modem.getSimStatus() != 3) {
		modem.simUnlock(GSM_PIN);
	}

}

void loop_a6() {

	SerialMon.print("Waiting for network...");
	if (!modem.waitForNetwork(240000L)) {
		SerialMon.println(" fail");
		delay(10000);
		return;
	}
	SerialMon.println(" OK");

	if (modem.isNetworkConnected()) {
		SerialMon.println("Network connected");
	}


	SerialMon.print(F("Connecting to "));
	SerialMon.print(apn);
	if (!modem.gprsConnect(apn, gprsUser, gprsPass)) {
		SerialMon.println(" fail");
		delay(10000);
		return;
	}
	SerialMon.println(" OK");


	SerialMon.print("Connecting to ");
	SerialMon.println(server);
	if (!client.connect(server, port)) {
		SerialMon.println(" fail");
		delay(10000);
		return;
	}
	SerialMon.println(" OK");

	// Make a HTTP GET request:
	SerialMon.println("Performing HTTP GET request...");
	client.print(String("GET ") + resource_2 + " HTTP/1.1\r\n");
	client.print(String("Host: ") + server + "\r\n");
	client.print("Connection: close\r\n\r\n");
	client.println();

	unsigned long timeout = millis();
	while (client.connected() && millis() - timeout < 10000L) {
		// Print available data
		while (client.available()) {
			char c = client.read();
			SerialMon.print(c);
			timeout = millis();
		}
	}
	SerialMon.println();

	// Shutdown

	client.stop();
	SerialMon.println(F("Server disconnected"));

	modem.gprsDisconnect();
	SerialMon.println(F("GPRS disconnected"));

	// Do nothing forevermore
	//TODO sleep
	while (true) {
		delay(1000);
	}
}
