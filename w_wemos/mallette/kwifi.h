/*
 * kwifi.h
 *  Wrapper for WIFI
 *  Created on: 24 avr. 2021
 *      Author: pfister@connecthive.com
 */

#ifndef KWIFI_H_
#define KWIFI_H_


#ifdef __cplusplus
extern "C" {
#endif

  void disable_wifi();
  bool autoconnect();
  void getMacAddress();

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* KWIFI_H_ */
