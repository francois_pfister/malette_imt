/*
 * lcd.cpp
 *  Wrapper for LCD
 *  Created on: 20 oct. 2017
 *      Author: pfister@connecthive.com
 */

#include <Arduino.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include "lcd.h"


#define LOG_LCD_NO

LiquidCrystal_I2C lcd(0x27, D4, D5); // @suppress("Abstract class cannot be instantiated")

void setupLcd() {

	lcd.init();                      // initialize the lcd
	lcd.backlight();
	lcd.setCursor(0, 0);
	lcd.print("started");
}

void lcdLoop() {
	//nothing
}

void lcdWrite(int x, int y, const char* message, int trace) {
	lcd.setCursor(x, y);
	lcd.print(message);
}

void lcdMessage(const char* mesg1, const char* mesg2) {
	char m1[] = "                ";
	sprintf(m1, "%16s", mesg1);
	lcdWrite(0, 0, m1, 30);
	char m2[] = "                ";
	sprintf(m2, "%16s", mesg2);
	lcdWrite(0, 1, m2, 31);
}

void lcdMessage2(const char* mesg1, String mesg2_) {
	char m1[] = "                ";
	sprintf(m1, "%16s", mesg1);
	lcdWrite(0, 0, m1, 30);
	char m2[] = "                ";
	mesg2_.toCharArray(m2, 16);
	sprintf(m2, "%16s", m2);
	lcdWrite(0, 1, m2, 31);
}
