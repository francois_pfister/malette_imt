/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 ** This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * COPYRIGHT(c) 2018 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"
#include "adc.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#define AC6
#define KEIL_NO
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
char Rx_indx, Rx_Buffer[100], buffer[100];
uint8_t len = 0, Transfer_cplt = 0, Rx_data[2];

uint32_t ADC_Data;
float adc_read = 0.f;
int wait_delay = 10;
char current_button = 'x';
int cnt = 0;
int bpressed = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

#if defined(KEIL)
int fputc(int ch, FILE *f) {
	HAL_UART_Transmit(&huart2, (uint8_t *) &ch, 1, 1000);
	return ch;
}
#elif defined(AC6)
int _write(int file, char *outgoing, int len) {
	HAL_UART_Transmit(&huart2, outgoing, len, 100);
	//while (HAL_UART_GetState(&huart2) == HAL_UART_STATE_BUSY_TX || HAL_UART_GetState(&huart2) == HAL_UART_STATE_BUSY_TX_RX);
	return len;
}
#endif

void printFloat(float valu) {
	char *sign = (valu < 0) ? "-" : "";
	float abs = (valu < 0) ? -valu : valu;
	int vint = abs;                  // Get the integer (678).
	float frac = abs - vint;      // Get fraction (0.0123).
	int frac2 = trunc(frac * 10000);  // Turn into integer (123).
	printf("%s%d.%04d", sign, vint, frac2);
}

void testoutp() {
	HAL_GPIO_TogglePin(R1_GPIO_Port, R1_Pin); //Toggle LED
	HAL_GPIO_TogglePin(R2_GPIO_Port, R2_Pin); //Toggle LED
	HAL_GPIO_TogglePin(R3_GPIO_Port, R3_Pin); //Toggle LED
	HAL_GPIO_TogglePin(R4_GPIO_Port, R4_Pin); //Toggle LED
}

void testinp() {
	GPIO_PinState st = HAL_GPIO_ReadPin(C1_GPIO_Port, C1_Pin);
	if (st == GPIO_PIN_RESET) {
		wait_delay = 1000;
	}
	st = HAL_GPIO_ReadPin(C2_GPIO_Port, C2_Pin);
	if (st == GPIO_PIN_RESET) {
		wait_delay = 1000;
	}
	st = HAL_GPIO_ReadPin(C3_GPIO_Port, C3_Pin);
	if (st == GPIO_PIN_RESET) {
		wait_delay = 1000;
	}
	st = HAL_GPIO_ReadPin(C4_GPIO_Port, C4_Pin);
	if (st == GPIO_PIN_RESET) {
		wait_delay = 1000;
	}
}

void writeDigital(int out, GPIO_PinState value) {
	switch (out) {
	case 1:
		HAL_GPIO_WritePin(R1_GPIO_Port, R1_Pin, value);
		break;
	case 2:
		HAL_GPIO_WritePin(R2_GPIO_Port, R2_Pin, value);
		break;
	case 3:
		HAL_GPIO_WritePin(R3_GPIO_Port, R3_Pin, value);
		break;
	case 4:
		HAL_GPIO_WritePin(R4_GPIO_Port, R4_Pin, value);
	}
}

GPIO_PinState readDigital(int inp) {
	switch (inp) {
	case 1:
		return HAL_GPIO_ReadPin(C1_GPIO_Port, C1_Pin);
	case 2:
		return HAL_GPIO_ReadPin(C2_GPIO_Port, C2_Pin);
	case 3:
		return HAL_GPIO_ReadPin(C3_GPIO_Port, C3_Pin);
	case 4:
		return HAL_GPIO_ReadPin(C4_GPIO_Port, C4_Pin);
	}
}

char kbd() {
	writeDigital(1, GPIO_PIN_RESET);
	writeDigital(2, GPIO_PIN_SET);
	writeDigital(3, GPIO_PIN_SET);
	writeDigital(4, GPIO_PIN_SET);
	if (readDigital(1) == GPIO_PIN_RESET)
		return '1';
	else if (readDigital(2) == GPIO_PIN_RESET)
		return '2';
	else if (readDigital(3) == GPIO_PIN_RESET)
		return '3';
	else if (readDigital(4) == GPIO_PIN_RESET)
		return 'A';
	writeDigital(1, GPIO_PIN_SET);
	writeDigital(2, GPIO_PIN_RESET);
	writeDigital(3, GPIO_PIN_SET);
	writeDigital(4, GPIO_PIN_SET);
	if (readDigital(1) == GPIO_PIN_RESET)
		return '4';
	else if (readDigital(2) == GPIO_PIN_RESET)
		return '1'; //'5';
	else if (readDigital(3) == GPIO_PIN_RESET)
		return '2'; //'6';
	else if (readDigital(4) == GPIO_PIN_RESET)
		return '3'; //'B';
	writeDigital(1, GPIO_PIN_SET);
	writeDigital(2, GPIO_PIN_SET);
	writeDigital(3, GPIO_PIN_RESET);
	writeDigital(4, GPIO_PIN_SET);
	if (readDigital(1) == GPIO_PIN_RESET)
		return '7';
	else if (readDigital(2) == GPIO_PIN_RESET)
		return '4'; //'8';
	else if (readDigital(3) == GPIO_PIN_RESET)
		return '5'; //'9';
	else if (readDigital(4) == GPIO_PIN_RESET)
		return '6'; //'C';
	writeDigital(1, GPIO_PIN_SET);
	writeDigital(2, GPIO_PIN_SET);
	writeDigital(3, GPIO_PIN_SET);
	writeDigital(4, GPIO_PIN_RESET);
	if (readDigital(1) == GPIO_PIN_RESET)
		return '*';
	else if (readDigital(2) == GPIO_PIN_RESET)
		return '7'; //'0';
	else if (readDigital(3) == GPIO_PIN_RESET)
		return '8'; //'#';
	else if (readDigital(4) == GPIO_PIN_RESET)
		return '9'; //'D';
	return 'x';
}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
//Interrupt callback routine
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	int i;

	if (huart->Instance == USART2)	//current UART
	{
		if (Rx_indx == 0) {
			for (i = 0; i < 100; i++)
				Rx_Buffer[i] = 0;
		}	//clear Rx_Buffer before receiving new data

		if (Rx_data[0] != 13)//if received data different from ascii 10 (enter)
				{
			Rx_Buffer[Rx_indx++] = Rx_data[0];	//add data to Rx_Buffer
		} else			//if received data = 10
		{
			Rx_indx = 0;
			Transfer_cplt = 1;		//transfer complete, data is ready to read
		}

		HAL_UART_Receive_IT(&huart2, Rx_data, 1);//activate UART receive interrupt every time

	}
}

void printAnalog() {

#if defined(KEIL)
			printf("adc_read = %.4f V\n", adc_read);
#elif defined(AC6)
			printf("adc_read=");
			printFloat(adc_read);
			printf("\r\n");
#endif

}

void readAnalog() {
	if (HAL_ADC_PollForConversion(&hadc1, 1000) == HAL_OK) {
		ADC_Data = HAL_ADC_GetValue(&hadc1);
		float read = ADC_Data * (3.3 / 4095);
		if (read != adc_read) {
			adc_read = read;
            printAnalog();
		}
	}
}

void readSerial() {
	if (Transfer_cplt != 0) {
		Transfer_cplt = 0;
		sprintf(buffer, "%s\n", Rx_Buffer);
		len = strlen(buffer);
		printf("la valeur est : %s\r\n", buffer);
		HAL_Delay(500);
	}
}



void readBlueButton() {
	GPIO_PinState st = HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin);
	if (st == GPIO_PIN_RESET) {
		if (bpressed == 0) {
			bpressed = 1;
			sprintf(buffer, "button=true   \r\n");

			printf(buffer);
		}
	} else
		bpressed = 0;
}

void readKeyBoard() {
	char kb = kbd();
	if (current_button != kb) {
		current_button = kb;
		sprintf(buffer, "button_read=%x\r\n", current_button);
		printf(buffer);
		/*
		 buffer[0] = k;
		 buffer[1] = '\n';
		 buffer[2] = 0;
		 printf(buffer);*/
	}
}

void toggleLed() {
	HAL_Delay(100);
	HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin); //Toggle LED
}

/**
 * @brief  The application entry point.
 *
 * @retval None
 */
int main(void) {
	HAL_Init();
	SystemClock_Config();
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_ADC1_Init();
	HAL_UART_Receive_IT(&huart2, Rx_data, 1); // activate uart rx interrupt every time receiving 1 byte
	HAL_ADC_Start(&hadc1);
	printf("starting=nucleo\r\n");
	while (1) {
		readSerial();
		if (cnt++ % 255 == 0) {
			readAnalog();
			toggleLed();
		}
		readBlueButton();
		readKeyBoard();
		HAL_Delay(wait_delay);
	}
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
	RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2
			| RCC_PERIPHCLK_ADC12;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV1;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
